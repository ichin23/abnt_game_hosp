'use strict';
const MANIFEST = 'flutter-app-manifest';
const TEMP = 'flutter-temp-cache';
const CACHE_NAME = 'flutter-app-cache';

const RESOURCES = {"canvaskit/skwasm.worker.js": "19659053a277272607529ef87acf9d8a",
"canvaskit/canvaskit.js": "76f7d822f42397160c5dfc69cbc9b2de",
"canvaskit/chromium/canvaskit.js": "8c8392ce4a4364cbb240aa09b5652e05",
"canvaskit/chromium/canvaskit.wasm": "fc18c3010856029414b70cae1afc5cd9",
"canvaskit/canvaskit.wasm": "f48eaf57cada79163ec6dec7929486ea",
"canvaskit/skwasm.js": "1df4d741f441fa1a4d10530ced463ef8",
"canvaskit/skwasm.wasm": "6711032e17bf49924b2b001cef0d3ea3",
"assets/packages/material_design_icons_flutter/lib/fonts/materialdesignicons-webfont.ttf": "dd74f11e425603c7adb66100f161b2a5",
"assets/packages/im_stepper/assets/me.jpg": "487511e754834bdf2e6771376d59707e",
"assets/packages/cupertino_icons/assets/CupertinoIcons.ttf": "6d342eb68f170c97609e9da345464e5e",
"assets/NOTICES": "2dcbb62ec7a173ce1a1f93bdacea3d3a",
"assets/AssetManifest.json": "d52bbddb0b41ccbbbc87e9f266cd8bf5",
"assets/src/images/quadro.png": "f18708a65b0af7c11aebec223fc3e5a9",
"assets/src/images/cnpq.png": "221f864bc7df62beb59b1e32c2909a41",
"assets/src/images/abnt_codigo.png": "2b9ba8b9958a331317acaa7eb3bdf638",
"assets/src/images/estrelas.png": "8673c869f4574d4bacb873847bad7a83",
"assets/src/images/mesa.png": "c5f92f95aae59f1e84e768d9951f2c09",
"assets/src/images/img-default.jpg": "ea0d26c409605c6e3acf0a15a8b812d7",
"assets/src/images/folhaFinal.png": "77a9a77bb2640f8965960b060ed27562",
"assets/src/images/coruja_login.png": "00304b95b76ddb41bca95c94382a08b1",
"assets/src/images/fapemig.png": "6743befa6a0b0f4f67a94a4593ff4d77",
"assets/src/images/folha.png": "2026d4eb334f2b175cd9e8937c4537af",
"assets/src/images/cefet.png": "ec02b1d9b2b7148195e64a1137ad7022",
"assets/src/images/profept.png": "b296bc9762a985e543c1c97f02df81ff",
"assets/src/fonts/Righteous/Righteous-Regular.ttf": "77fa00996ecb4104c7880b8749c7c4e0",
"assets/src/fonts/BebasNeue/BebasNeue-Regular.ttf": "b2b293064f557c41411aac04d6f6710d",
"assets/src/fonts/PassionOne/PassionOne-Regular.ttf": "072e0d0fe8689a237ed00cfe5c9f39bc",
"assets/src/fonts/PassionOne/PassionOne-Black.ttf": "ad702d5d1d2f7495c5d1b60aec91c68b",
"assets/src/fonts/PassionOne/PassionOne-Bold.ttf": "20df97aa8f4d50499916be38d63fe540",
"assets/src/fonts/Montserrat/Montserrat.ttf": "52a37115b1d8d5d6ae0b0e373e692c9d",
"assets/src/fonts/Montserrat/Montserrat-Italic.ttf": "5b315bd5a3b7fa34eef7dfd5786b90b0",
"assets/shaders/ink_sparkle.frag": "f8b80e740d33eb157090be4e995febdf",
"assets/AssetManifest.smcbin": "c9107cc1544cad9288eeff19eaffe90f",
"assets/AssetManifest.bin": "b9eb03da04c372b1fdfd899406a1a9d9",
"assets/fonts/MaterialIcons-Regular.otf": "e7069dfd19b331be16bed984668fe080",
"assets/FontManifest.json": "74b848912171c4193da77618eccac63d",
"main.dart.js": "6a990daf487e654b21fce5c5a83c1d16",
".git/HEAD": "cf7dd3ce51958c5f13fece957cc417fb",
".git/ORIG_HEAD": "56ffb5355ab567b494c14e24e2e5f588",
".git/description": "a0a7c3fff21f2aea3cfa1d0316dd816c",
".git/hooks/post-update.sample": "2b7ea5cee3c49ff53d41e00785eb974c",
".git/hooks/push-to-checkout.sample": "c7ab00c7784efeadad3ae9b228d4b4db",
".git/hooks/pre-receive.sample": "2ad18ec82c20af7b5926ed9cea6aeedd",
".git/hooks/pre-applypatch.sample": "054f9ffb8bfe04a599751cc757226dda",
".git/hooks/commit-msg.sample": "579a3c1e12a1e74a98169175fb913012",
".git/hooks/applypatch-msg.sample": "ce562e08d8098926a3862fc6e7905199",
".git/hooks/fsmonitor-watchman.sample": "a0b2633a2c8e97501610bd3f73da66fc",
".git/hooks/pre-push.sample": "2c642152299a94e05ea26eae11993b13",
".git/hooks/update.sample": "647ae13c682f7827c22f5fc08a03674e",
".git/hooks/pre-rebase.sample": "56e45f2bcbc8226d2b4200f7c46371bf",
".git/hooks/prepare-commit-msg.sample": "2b5c047bdb474555e1787db32b2d2fc5",
".git/hooks/pre-merge-commit.sample": "39cb268e2a85d436b9eb6f47614c3cbc",
".git/hooks/pre-commit.sample": "305eadbbcd6f6d2567e033ad12aabbc4",
".git/info/exclude": "036208b4a1ab4a235d75c181e685e5a3",
".git/logs/HEAD": "1c877f1633e8a6aa091245127bb7e79f",
".git/logs/refs/heads/main": "ccf93f032a4ac733648d32e00d98c123",
".git/logs/refs/remotes/heroku/main": "793268fd88b1c64dda3420f42e1f2b24",
".git/logs/refs/remotes/origin/main": "6860919f87726d5df86707349226a2d2",
".git/index": "d11138809d6af25284c832708875857e",
".git/refs/heads/main": "7f55164b6d87247360f048a38cd23c1d",
".git/refs/remotes/heroku/main": "ab19e48d8a01257f29c190298c366795",
".git/refs/remotes/origin/main": "7f55164b6d87247360f048a38cd23c1d",
".git/FETCH_HEAD": "04dbe4076ad35f40b65511f9007c8807",
".git/config": "a10a82e4aab2e481586fbe0662257e27",
".git/objects/56/079ab980b1a26c2df965d15256c6c9af3ef028": "0504aa5f8a0493e3caca82358ec1ec16",
".git/objects/56/9256b9d95207e5296132564b17c725ff2da777": "9ea5fe88903ee1bf586eebc3321a08dd",
".git/objects/45/6b6302535a1525b9423cc896ca1217c3565765": "d28953b79bf182f54d5d9ba7f8ae854d",
".git/objects/45/bb57f68da95c18a9eae92544bfb0c513c0ff8b": "c3d1bb9fb017e59c5ac3dd7a8bdd4563",
".git/objects/45/8ff2fd3056ad5772c6c668f2d4150f8fa33dd9": "db24b71c0caaf2935bf2612b1e2e9af0",
".git/objects/45/fafafaefd7e639f4146f0bbe05cad4425d3ae8": "79b79325c4aadddc2fdc8b94a5cd701d",
".git/objects/45/6dbaf4badd0e56f923afcb0af7034a27b3c5bf": "80ca3a40634b3ddcef140f6d1454aa7d",
".git/objects/80/9656ef3424ff3601c120267d748de86bcd98c3": "2029d517ab0d78d7e3457b2ed3ea361e",
".git/objects/80/f072634ee20021017629a80f43ddeb419dcffe": "426b3ef1a6621e7dca3c6f2c01c385a4",
".git/objects/d0/d67e5a682d700b0183dcc739bd813e32be7a33": "ad4e65b1a77c859fce3438bd1a3c03d6",
".git/objects/f3/987291d894bd4541965728f72cb57f08528907": "71f5c15c32688c9bfe0a5822e5f45ddb",
".git/objects/c6/867eedcc64e72048f326f4253884459f6ec639": "e646a478f681cd5a5aaec4dfdc2fa587",
".git/objects/7b/c81f4c9b5237ae1b3c05b98694c1cbef07a01b": "bc282d2ca9e1747230b9ed2bc2734289",
".git/objects/40/88d5374c4a0c4e5c561408f62f97fe4462d857": "cb1a2c1c539d4988548b879da6ac59ba",
".git/objects/40/5858eaf762927da0ec62c2b5b11984a7420832": "26ea3809d1b97c4908590ea4e3e875f5",
".git/objects/40/5918b10a118fa54d834d4c3fadea3a6bcf9c1c": "bc38be73e50892810165cb3257e9c654",
".git/objects/bb/a3b5ac5d369826c0c421ee0234b4bd35037e65": "cf7026d083ed954622003b773f600c46",
".git/objects/bb/ac29f5ef7a40bf14c0901bc1457724156bc0de": "1393f20f0610cabefe2d4f45865b0f54",
".git/objects/bb/1d725eb41a9ccb3ea587876b37c68f1f2bf524": "fa8f488864206dfdbf334f64a32b3c6e",
".git/objects/bb/0149bb88ce32e27394a5f4812c0889b9fa83f6": "7206f571a8d8b04843d1ff73888042ff",
".git/objects/0f/61337cb07d390caa5376ba34d1bf2bd4bdef77": "3bff96175bc0c8a0f09810fa95f4f9d6",
".git/objects/ae/6422ecc7e6e8a5cf355092ca79192b5b37babe": "4f704f617d4f79258c41eed211dcb16e",
".git/objects/ae/a87851743d87c8e0c36455fc90ca1aee7f6b3e": "5021f612f26e6f67c5663f47982f0da2",
".git/objects/d3/efa7fd80d9d345a1ad0aaa2e690c38f65f4d4e": "610858a6464fa97567f7cce3b11d9508",
".git/objects/d3/c45fcc7552d63c825ffed886c872793584087e": "da322cf8cc3d3971a8deb805d539ce95",
".git/objects/02/0d54d3779f65f6969968bd2c79f1b9a17dbdc8": "5361bc655363b3745c747fb245b099fe",
".git/objects/a1/ee3d6edee8c23eb09bd89d428ecd4d5faebb1a": "11900b806f601339da37965eaca6cc0d",
".git/objects/a1/3837a12450aceaa5c8e807c32e781831d67a8f": "bfe4910ea01eb3d69e9520c3b42a0adf",
".git/objects/a1/560d9a156eb445174f9575d9d3831d0acfd1c0": "fe9f840bf49b623d3a9ab2321ed6c0fe",
".git/objects/bd/cbb78d0ea347281cf9331708907df9be168ad6": "e4349fc90aff2c8275a8d7725b170b5f",
".git/objects/bd/56666eb437545be44d8024fc955166e4b9483c": "b98d882e371c7232b47828ddab096464",
".git/objects/25/a35e453ed4ed792204e44ba2f6378eb52b8903": "1ae15eec8f3c9f4e52861fffc8100f93",
".git/objects/a4/eb0fb2274dec86211b4e6dc06c20b6738c08bf": "f68cbe1d617bb58d96f816bee25c5f72",
".git/objects/ad/a574644d8af51932e96de812563e06db897846": "a4f0a22f62999588d13f7093154f8d77",
".git/objects/07/0733459e5163528c6324038cf8f490d6af20cd": "b130875fdca971e61c61cd94c3e7fdfc",
".git/objects/eb/9b4d76e525556d5d89141648c724331630325d": "37c0954235cbe27c4d93e74fe9a578ef",
".git/objects/79/ba7ea0836b93b3f178067bcd0a0945dbc26b3f": "f3e31aec622d6cf63f619aa3a6023103",
".git/objects/8a/aa46ac1ae21512746f852a42ba87e4165dfdd1": "1d8820d345e38b30de033aa4b5a23e7b",
".git/objects/5d/51d35e924bb7547f2146cdce7fc76a3edcebca": "0f865d78da815e8ef2abf26e9b3f2b60",
".git/objects/1e/bf993c04c08e17a0122730f8d7ce6e139c8bad": "eeb4f0d71f24758335fe1753273ad6c2",
".git/objects/f9/3e3a1a1525fb5b91020da86e44810c87a2d7bc": "3bfabf2b9b9d1a341e4263f23c96a46b",
".git/objects/69/ecb33fce9468cc65d841eaa6f1cca58aa8ef56": "2acd0b2a8c90ff1c7db2dc1459cd9a7b",
".git/objects/69/fca7582fea3889e2b44376dfcffe3ce17afad8": "c2862a7461b3e23cabfe7365f6383e96",
".git/objects/81/777ddea2cf0f8df3edc1324fda5a7f944c3751": "3e0073d2c04567ac632720370049386f",
".git/objects/26/43229181f8b4ab3f55ed67747d2ac7baf0b30f": "0950a42be79ea3f7379e5aa374013ce2",
".git/objects/e3/2a3dcb62a5d1b1ff7e266d63c239b499c03048": "108bb0777c47116cad83aea216284189",
".git/objects/70/010cc4761157d9d7cc2d082cf342e63fe1190a": "baf21d1dacab382149ee93266543ff40",
".git/objects/65/0b1e44261d3e122b5dcec04f34bf6e4c919b39": "1ec0e9030f56111043a220c1b3e17479",
".git/objects/65/6db66684354d3b5bca92e5c395e1ffcdf4a472": "2ff96512a6010bfd397902a11d325c79",
".git/objects/dc/ba1f3c86a8b31b412a0884ca83052a574ed0db": "f3bd16ec47a5d711503f014062db3ab5",
".git/objects/dc/4d13af8f4fcbbac2aa6461ed0734b0262d47cf": "94f9cb6dac41400ed08e7796553a9932",
".git/objects/53/26d5e3853f18325c89e839e0e299b53342152b": "00ac0ce13a94acb6304386088ba6cb97",
".git/objects/53/b94d5c2d3e7a5e136d28395a5ad157b1fbf471": "ded2ae1c939b17731d5068272161f0c6",
".git/objects/e0/55b6bc548cc2f14f1d0329af3116d1602c1512": "bc47b7d2456228490ae1d51c75c9f5f4",
".git/objects/82/d318cb1300b91a275a23d7f0f076587bdb0b4d": "e8ca0ff14ef68dd5348e2d979402bcec",
".git/objects/58/1099819730ea534c1049c8f1dc367c03c8f2f7": "fb055e0b3bd174df87e0529ea8b87fbc",
".git/objects/08/d3b25d9af275ac2512b5841503321224347a30": "4662d7b7e2d8c27a2c05762efd1a2ae5",
".git/objects/43/2c74d719628d7281ec904dd2fe6a772dc91dc5": "1e46b99669e93f07c89ed613bb68f84e",
".git/objects/43/2b3bb42dc1c9c19755b82161c070848ac15585": "aa8565bcd73d6920d1923b4dd912424c",
".git/objects/b9/2a0d854da9a8f73216c4a0ef07a0f0a44e4373": "f62d1eb7f51165e2a6d2ef1921f976f3",
".git/objects/b9/e7e73385273068a27fdb370b755fa6bff92955": "c3fbf78dc20f499ea1e2af2d2834beb6",
".git/objects/fd/f9d9f94999cfe09a55ba79278b0c2289eb4a1c": "6b2cc9f267565d0d0a421b0cebd94d9a",
".git/objects/fd/bdbca2663831a1e49ccf2144246a6e721579e7": "3cce17d640688e0e5276b592980d38bf",
".git/objects/83/d18e92906648a1bd92ab0024403b1a3a8e32ca": "405e574c40980bba4c4095b2589feebf",
".git/objects/30/5aa6736f3143fb8dd5960a47c48663b0d4fa9d": "bdb554f971b943482fc94ec72f080faa",
".git/objects/30/e7225084dfb3f429ed5de47380620d70121f20": "e4d4a33cadb548188148e2842041c72a",
".git/objects/30/edd503544a137f4989f15b704da9e3051d19a4": "a0538e6b4ade9b1e143055db448208f7",
".git/objects/9c/397d2d83af37ee630d43b24a3b6e0479904185": "ea1fadb160a1822b529729d1d6efd9b2",
".git/objects/c3/694e0e17bcea2817cd9e202a81075520b048ed": "83c1982a1ce7f35c7ce65e6f4b6e23bd",
".git/objects/c3/5bc060aa61bfc425df2cf8b5ef5e2edb374120": "9ed1671d3e6e478654d55162776e3d26",
".git/objects/d2/dd287f1026edcfdabbb5be2b588836b589543e": "67ad737c008fe81f44ea791a425980c1",
".git/objects/d2/190b5139486ecf8c655e1cc1d06e8df3ffe249": "38d744ba160ce93006e81e51ecaed9b8",
".git/objects/d7/6eba6e873f0914d9a053580ca2ec8ab046940a": "0ca5f6ba67ca8f78fe2446e367360f8a",
".git/objects/0b/523c8816a2a44f70dda9ebb0580b0fb893004f": "401b66c88c590c2ea2b329978a436e9a",
".git/objects/0b/03182747679abf00fd0226c5f419005d3dac1e": "3095cc1948769ef921c96de31ce2ec78",
".git/objects/0b/85bcdb86bf9e9f9fda81b13cec9c9349d47d77": "77cbf4b6cc88e2471afd14a98ef2e0ed",
".git/objects/4f/22aa516a45e3af9701f514a88202b20e5b9451": "56d63db371575ab59e0dd23156c268f0",
".git/objects/13/dd70993c3098999dd982e54cd79492133dba5e": "991b7e75ed1bf2d74700c39e84bfa4b1",
".git/objects/2e/284cc6c68a6f46b78edc81f3727882606c45b9": "5b68812367fbbe63d3b77ebaf0bfae0d",
".git/objects/ab/5857582a9ba6f8b5de1b5ab336bb2968a989dc": "e3f3f7dca918195d463cabcaa835b092",
".git/objects/ab/c494c26bf601a2cab163420b0be9e08843b690": "2ac5d032cd343cf08b7072e770b556ec",
".git/objects/d1/58e2ae775fddbbb691e731e9cda7873884c622": "76911a287bef8c405bbbb1d25396f5b4",
".git/objects/99/1b0cc784d9d3b9e9749210fe774aff93ca8f82": "fa7333ecd61241030c1f4dce0bbf24e1",
".git/objects/99/c96b113ec7de8d9f7bc9f27a25ecaa049b1ecc": "cca18649a23d377c881e1424ea27e68b",
".git/objects/6b/9676ecd8c08508621e0584d19f4877504fad76": "d9dfbfb3e77fb3f00e893f1d2f5d1fe0",
".git/objects/35/22c25ca3f9ab93d4432e0cbb5546c101f42e4e": "a4400df8773482a3b667277ae171fb21",
".git/objects/35/91af41948adc8001f3586d76b91181311953fc": "c91d33b29071dcff3b2b3385383761cb",
".git/objects/03/c531a7a4eadfbb97e5434401927ee31890cf33": "fe6b24f60048b7f218835d95c2c650ae",
".git/objects/03/726d61363849c2a34978ae59a715f3a2f5d288": "414e002c08492fd071621c8bd44efc94",
".git/objects/e6/9de29bb2d1d6434b8b29ae775ad8c2e48c5391": "c70c34cbeefd40e7c0149b7a0c2c64c2",
".git/objects/e6/e4e9499d0bfb11686ef9deaa2061ca5853da68": "c89339d819621c4ebe53568babb43966",
".git/objects/d6/9c56691fbdb0b7efa65097c7cc1edac12a6d3e": "868ce37a3a78b0606713733248a2f579",
".git/objects/d6/85510874af238852c0c9fec81b9c5b0f870fce": "ae2fde46017d827b97ba52ebae2b75b9",
".git/objects/e9/d90ce889d67555feecb78d94635b7b41e294bc": "d3267c3c353b6297718c561f0f2749ce",
".git/objects/fe/d959c2d490d9c23ac95e0e2cf1a308ac3faefe": "9200a8e19eebaad7aee31593ec970ffb",
".git/objects/fe/69312c5e70897658f28dbf954f88630a93ca81": "78aa2e0c1a3298bd8afd016c3bcd78d9",
".git/objects/51/34e6402246228fb7f58ce8fe76727a61d99a62": "6b5e5b48febe40daec7062aecdc3b39f",
".git/objects/d5/97756a879937c3d61962a75bedc52ff7e791f2": "01cae4940a53f2708328761ba571369b",
".git/objects/d5/b54bd4a898b373f82bb1fa52b9580e7a976e3e": "943e27e1d359e2bc22daf20c70287c19",
".git/objects/42/626ab0804cea8d53e8e047aea619535308beaa": "a60cee1109a6c83661cdcfbd264a0dab",
".git/objects/c4/2382e02818a48e4fdee4e154beb6fb4430624e": "3be171e87fb627538328d432c6b336dd",
".git/objects/0a/9b2d2f0cc5e4d25474bb3496bfa2f60072336e": "5f893daad44c25c7e5990e3ac21edc3e",
".git/objects/9e/357e1a4a8660542444e0efb96bc96884d5785e": "02040bd56b088a0c35246fcf40041b9c",
".git/objects/71/427366792e651ef480f67872c5dfb3cf14d479": "69b40da78c000b4c0c841f577158d81e",
".git/objects/47/018468b53ca744df83d57310c12eb191890736": "2617891b6f05720bcc68ac0e50f527cb",
".git/objects/2d/dbbedcc1addd098de0eedb216b0af909709760": "df818539e9378212020b20affc09fa09",
".git/objects/7f/54182886d8f74bfd19e9673d51802804162e96": "fa8454849c33400a17aae3791e4afea6",
".git/objects/e1/7edb1a9e3cec722c0b6b540697b6dd5a9c7e8c": "1d859e14dfa80e74157574ba30e04a01",
".git/objects/e1/0c188d7be9d7ccc98cfe25d39ddf197d4ba3f4": "1bb5df0a4913335b407a24ab0ab3ca5d",
".git/objects/cc/30d6c974cefebbb6666f51ac03aeb80a554eb9": "1068587830caecb65f44fe2ad573fc93",
".git/objects/62/a01d6826913d9efa140d2e9f4bc0f13918e607": "44ba2af6a4f05cb190463143170ae010",
".git/objects/62/b6b27a1de27d46a33202e7d2d1f32fcbfc8ef8": "f596239e851daffc3e6230b4b4c6d1f0",
".git/objects/62/d20400c955f5fd6f88668d4a43a533d10d134f": "8be73b7a34f0832b2d24445d681ea189",
".git/objects/91/b16d35d608b1bf5792480d5dbadf91a33912b5": "d3fb8b4cf26bbe9eb43a47e7247f1bf1",
".git/objects/00/56560efff0b7c4da06f6e0f6f341eaff4b0c29": "90c25866296f1c296a7c4be9ae541633",
".git/objects/9a/e9e0542acc08363eb612898fd73e6c79e27b39": "e77fdd1fa3b918ece03eb6c7aabc2ef0",
".git/objects/9a/2384d23be41590efd32abf064815435655304c": "1a0f78d96f97315b240a26860dcdf296",
".git/objects/9a/40c8f0f3d2c62968821ead92704463c8bec688": "6178ae626bccfd1ca89c8f4365c0de30",
".git/objects/ca/3da6102e11e364e27cefb04d3c95ffc462297b": "42fafb9d271c4e939ab9d958b5b80ada",
".git/objects/a6/c11d5c815f411ea27bad974b23d60e01ff5fe5": "ad09c4ec1491e4ae1f6a4e108a015922",
".git/objects/20/913378d913b70bfbad56106e75c5f6a0891142": "67c151baf142c702d3327039b735ec77",
".git/objects/8f/c45f32faa56c22480379a1546cd3f4ec1184f1": "586fdfb96000fe9e0c562506a8867862",
".git/objects/cd/cbe9ad6c96a7b51b383f6f3b345dc2ca0c1bf0": "79aabc8f6806112c30881db82c371656",
".git/objects/cd/a938a672a60cdf8c61e69991bd1ec044f32192": "70b62262cf146dcf194bc5687136321e",
".git/objects/cf/f61a72c9806b87a5d385770c31b58da96e98e1": "a3baacaae5710de599a6d2a3aa948ee4",
".git/objects/38/28d72071c41e9038133437412d66766536b306": "04cfa6ce8523d85da3128d07b41d4dd5",
".git/objects/3c/37d3306c36555531f3cf714a6fa8585549a811": "cb493b6fe4784698083e0c1c4ccfe42b",
".git/objects/3e/c9ce0d446b4cc325a71af27c5dd99af81c16e7": "b600961cb8d6e48908e15f7c469144b8",
".git/objects/b5/c8f0c40464917e8cff4b33672ec43e11c801c4": "78c7104ae337b71d3ca68576ee4724dc",
".git/objects/11/c96c928c1268c7e4ccc57c7150535ef868aeac": "434232c946376d4276f995b6fda033b9",
".git/objects/11/7bea37eadf0a902fdd63b00b0fe3149df624e6": "b62b281a6173ed5e34db50c0fb2b4ce1",
".git/objects/a5/1cb03585ced8a5348b9750a802108b0efdf7a7": "788a4c91a27b19cdc225469bdcb0e4b0",
".git/objects/a5/0dd9f0f1540f2e4898fbe9d37ee2b03c1d5903": "ede9fd98ec435bff9f9e741f16636464",
".git/objects/e5/485507eabc72c72b24982272df9d04b08632ab": "9cda10a1b19b36d7c326680b03b9e20d",
".git/objects/e5/217fa51b0aa0811122a1813b116e37eed86ff6": "b24bd17ad46396f68e6f418d8c27dc79",
".git/objects/e5/951dfb943474a56e611d9923405cd06c2dd28d": "c6fa51103d8db5478e1a43a661f6c68d",
".git/objects/b3/adf6c4bb20d1361a0876b6bcfc33bccc7afdb3": "11c14c4319f3aa6891301a52330c65b6",
".git/objects/b2/2fdb2d1fa6a3bced274617d58f6ab432bb0d8b": "1b405e4dfab487f51d41422d52600614",
".git/objects/d9/85fcf04a68de59214aac0b47cfc422e60ab7de": "faf1d6408fc53ef0b94520608480a7a7",
".git/objects/d9/2602907d57da7b543f3d252186a34b53f34c8f": "2c01a43a565fc72e033e00ae45837d73",
".git/objects/63/da91fb38e6b68122132a7c9d8a81d2264f7759": "5dd5c3ee23fb987ed28133a8cacb7a94",
".git/objects/63/7fbc28fefad6f0f0e551e6c8278c2d8f7afdcc": "b7527e975e19e22752575e0cb81d184f",
".git/objects/9b/bd4dbb25c30c78263320ce075cae57bb9ea5db": "9376ccdeafd411b3b2cb5391b071b5a3",
".git/objects/9b/fee9a2de3f41f413ed246b4361b2e9bb2a5486": "3b96c039bf309990ea6c4f721cd5af10",
".git/objects/0d/aa5d73fba455a0af13c899a5ce07add8074ca7": "d3247b15e4eeba6ee42efd42bc677253",
".git/objects/b1/090842259b1b3f1d15f2613781282275cf84e3": "6ba07d0628bd95804dd2ddd4957ff902",
".git/objects/31/978623f5119cc601468df4797965426474d83f": "6f06d35f6b62c27aa391d643f3bfb928",
".git/objects/ef/5acc4ffef5563cd3ab4399afc40e5e966e5821": "6099e9e238eb1d99e6006de5c38d7cda",
".git/objects/ef/d70572967a38cd46bceb6606b15a0afb9adfa4": "562f361d83bc8586ec2fd53f9ef66fde",
".git/objects/09/fa89ac76355e2e70d31c17b1adc827d36f1ac9": "f03e0c8e730519b3901f2fbedea2cb21",
".git/objects/09/67ef424bce6791893e9a57bb952f80fd536e93": "468eadae31baf8cd84e8ef7fc78d49ac",
".git/objects/09/9be0fc86b29052f283167fe539ac9f6333ca25": "885e1d62f7c876ad2e6aac52b18ae52e",
".git/objects/f7/253253de31db47247373ec7bdbcace1ee09580": "546e4bd2709994a0395be5e2e28a9f8c",
".git/objects/f5/e509544320de46c4fb0863c29210577e1a5df8": "b3a9821b18e234cf8929dd3f3fe7e2d4",
".git/objects/f5/fddb5f6cf93c79bdbb5363a10d539432eb394c": "e5fdb07ba9299e8bca2159fdde4bf349",
".git/objects/0e/2f7da6fefec0973bdca614001fc3ccdaef624b": "fde19aeaf08138b2cdb49faa9c3c1669",
".git/objects/27/9038fa34b51bdb50d5d466cb92af850111d463": "6ae85541ddc1a925ae80aba753f51466",
".git/objects/27/97f9acab372b8d37ceb684639e93af0a192193": "7db1e01b5a8f8c8661cd7db8ac05806a",
".git/objects/93/a59a50f35823fd6ff439d2b5eb943b0364f7b0": "7baecb8712cb72e417ce4585d462d6f7",
".git/objects/df/fb224594ee159158d8ac89ced9ac355b0e5d61": "e435f1f50f1d249ba189d4d2956959f4",
".git/objects/95/bcb7562bf2e564034b89d264d01c05a263feba": "9d50fb96b94fad85957ac24c5cbd78e3",
".git/objects/a7/f29b1ab6e11df38ec75aa3a84197135d2748a5": "84f67e3991c3f6f3798b8c7aedf4c52a",
".git/objects/1d/5faa486749d6df70dabe0b38992ad82dec5415": "b23808de464abcee03826e4f96906ad7",
".git/objects/04/df37c0fb0c68b1f9814c7f38b050c0d6a78e01": "d890544105c2c029b54ba1c3daaaf3f5",
".git/objects/39/ff7f18b484b077249de72a0f2d55618fbc2f50": "95f6a5c3c062ea7b925389750ee37d97",
".git/objects/54/953f7e4464a54b5c1b14ef48b4239c0d2b4f3f": "ef4bef0604d53557f8e8328bc2767018",
".git/objects/54/27686d0f6e444d59536af81be155eb1fdb93ca": "b6ecd3e8ba383935165b331fb3015c2c",
".git/objects/c7/a9f693d40ce2e5c1065e09ab362435342b0a25": "1902a9ae5858b090865b2e4b9c7b6367",
".git/objects/c7/79e04c2cb5601fbe936f497657807e9bb0cc13": "061f7e96c37ae096259d992d29c1e280",
".git/objects/ba/3a5cf826d592e3955ea58a19996c8df7975874": "8f855371b68d1d9683c4ff0b770f7d1a",
".git/objects/16/8e579f63d8da508c6ed6bf57b07b56b7b67847": "6a65fe7c13dae5cc1f3efd98b838b95e",
".git/objects/89/9daae3eafe8428ca1a18f08ffe2388aeca7b15": "4e3888e5dbdc487c46cb14c563b85adc",
".git/objects/7d/771f13017df7d226476c43c1dcd07425fb5d58": "1f9b515852e8e32ff829ea458d4b4fe0",
".git/objects/05/6b6d223d9f90505ccdba30236a8535e424cfa4": "4c7aa4753d1a30febff516121d7735f7",
".git/objects/fc/9c0a84cdd5eeb260f6a4559ec2356d3cff816b": "fb178e3ba249e335f2b9b2a944d20a5c",
".git/objects/fc/de1bb3df8c330568f07ef326d43d8ae3562897": "6e5bf2450330342c243afc3723b9c27e",
".git/objects/fc/a3ce93fec877d98765c91f081630c7ff3abebe": "acdd19c28f24ec782affe22669aa23c5",
".git/objects/1f/686edd1465272558af328ca43cb7189a0059e6": "5e83820f6d3e5392693d45bc239b2b61",
".git/objects/f8/e9396646d1d14fc233b213a94844b877982def": "e9aaa7574f33bbbd00d29c61724f4b40",
".git/objects/33/f2290f27550adb85316b4ddf8b2ca8aa6a9884": "bbfcf347bdf03d2c5de73e1b52fcb067",
".git/objects/41/5c059c8094b888b0159fdedfd4e3cb08a8028e": "86914685ccd40e82a7fe5b70459fb9f7",
".git/objects/37/7580cbf691d03aea79c63a3a251b1b48ac01f1": "c196d282a50e3c372b4445c6b8868297",
".git/objects/db/baae0a7b57664462b4fb3719ae90dd59c85cad": "92f3fa4ff88674bfbe3bcebd1535c0f5",
".git/objects/24/27de0d5a630edfb409a98edf47ccba70d46204": "d1945acc6604a6f19f6c20d2764ebf14",
".git/objects/24/fb3572489e913a3ad0d3a7be6281baf13f9e0b": "eefd8fae672c25afee2615fdef935c38",
".git/objects/94/cec89e472525e016ca84a93ae48de3613609ea": "589eb92fe20f9de302903f988da4c620",
".git/objects/94/5c223a4f319a0791dbd8b5abd09e3016916a65": "afcc8c7e8feaf5135eaf43d8f9412bd9",
".git/objects/22/f3f75feb5b3fd19d01d86b4be4633861831449": "9f533404088c5fa6ed319b4d5023c45e",
".git/objects/8c/99266130a89547b4344f47e08aacad473b14e0": "41375232ceba14f47b99f9d83708cb79",
".git/objects/8c/32bd5345bf0f4d07aaa08f26a2e1ddd7fb4402": "d99c1d23c2901bcf0aa3e3004f5bb308",
".git/objects/77/efb373b0f469f527e93c27edb88dc4205cb470": "d5268bd690bb6cf0c2f6946540092a35",
".git/objects/77/b6f867db0ef4444a706b0fa3261db9f0e184b8": "79cb737e4c29a447013c56550932284b",
".git/objects/ff/378e794cbff91fa0a259639a6f0b20fc003026": "cc4fc8e7a6650922323cb88a69526ee2",
".git/objects/9f/021f8fbe602eadc6497e77c2538032d8f14bf3": "daa792eff75585fddeb4e6a73ae59104",
".git/objects/9f/d0e63e656be8211ffe8fd878912783565dbe6d": "597a92ce43b5fc008d98e296e251182e",
".git/objects/b7/61008eeba7c5de5ec4bab7c0bcbe85d83a1e96": "8ecafeaf415b0eb3f5076f79bf183324",
".git/objects/b7/49bfef07473333cf1dd31e9eed89862a5d52aa": "36b4020dca303986cad10924774fb5dc",
".git/objects/a8/e3dfd0d97a9a6a6d64a0b9ffe64319f0badca3": "04eb5a5031bfb430032098a77b5527ff",
".git/objects/4c/6662baaa5824998e127fbcd6d6ba25d0ece158": "08696f2e0df5f440c7c0f3efa46382ee",
".git/objects/4b/c7815b8a263f2323921eaa32509015948ad644": "3f806b7bcf75388448ca99c497791c6d",
".git/objects/34/7d096672e147cfc0c99601e859d1bb98af1895": "add9607dfc403698863dad3b13cc4ff8",
".git/objects/34/97a0aa7b5cd8645a36bc8f66bcb9414cac3421": "a1b30e206c70e5ee4662f107adca87be",
".git/objects/34/ba3bb761ca68af02745672cd49004cf61cbb24": "4ccb71c9f94c9d3804d05d9c394968f9",
".git/objects/52/a92378e53171ca64156839ae0201f9bf53dfe7": "f7f72acd3650ebf51af66a4e13bb4bc6",
".git/objects/28/4f290df235c8404a33f43f49515c6a2c81cba8": "6a05ca3829cdfa1a67c1322349437b22",
".git/objects/01/17d50015d9275f2b34aedf1a371ef839e1238f": "ac124602be9030df8da533dcfe4fb6f5",
".git/objects/01/180b234d62960f7e49f38c508ba93aaad949fa": "54be0707e73e494df1d5e055c8958956",
".git/objects/29/9f7712fc9b0eb562888e70b868c38e503b5a9b": "33032e3bd34c82b7289bba83ce65d0f4",
".git/objects/29/13b125a48e7d2f4c4f5f6066dc48b7182fd3f1": "2f70a1e985b008ca9918b89da7815df1",
".git/objects/4d/40bb774fb0b64f61d0583fad9be07f9c95dc9d": "9c5819da5707e3dd1b0897af2d7adcd6",
".git/objects/4d/cf7e73d8b740be4f871c3ad66c13464565af44": "4c3dd70214b92ff20b2b4f68829b2ad8",
".git/objects/5c/ce7d0b88f8137d86cf384738b76199844d4384": "4614c47edd1e4cc25eb8c0e86cf721f4",
".git/objects/fa/8dbffaa86c8537a4c14549f6e7cf33566350bc": "eb1c3e5b1e035aa3594e10000039da30",
".git/objects/88/cfd48dff1169879ba46840804b412fe02fefd6": "e42aaae6a4cbfbc9f6326f1fa9e3380c",
".git/objects/88/bf76733a0cd459827bb31634cd1a51fa4b0bf9": "1b2c8001428f4e96400d505573e47e32",
".git/objects/fb/aa736e30df085954c3b6a0a0c9f592ed1bf1ab": "f8c29e299506a71818ae0fadaed49f6a",
".git/objects/d4/716fb951ac80acd2ed3666fd0398fe540d3af9": "e10f1179f4d673da2707cc5b0f995646",
".git/objects/14/5d533b90e247671973bbdd872ffd0f636399ba": "166931f81dc3fd046a1f7c0f38a529df",
".git/objects/06/dd0cfe3b192692184363cecae60483e0f2bf59": "e144580e79eda24a350816ec600d5cb7",
".git/COMMIT_EDITMSG": "e7f8f8a87a484796b97973ee42f152e2",
"README.md": "602fbab7e69ab22c5ca6224542b43b46",
"favicon.ico": "6328807800f07c39924aeda39edba5f0",
"index.php": "46ac990120d2b0d9f1c37034ae7a0667",
"flutter.js": "6fef97aeca90b426343ba6c5c9dc5d4a",
"favicon.png": "5dcef449791fa27946b3d35ad8803796",
"composer.json": "8a80554c91d9fca8acb82f023de02f11",
"manifest.json": "36a321dbaa39836d7842d471ac143b58",
"index.html": "1be7222ac3e068f2b025cfd929fbc56f",
"/": "1be7222ac3e068f2b025cfd929fbc56f",
"version.json": "9e30e72484bdf47f31a9a3f14ff6a627",
"icons/Icon-maskable-192.png": "c457ef57daa1d16f64b27b786ec2ea3c",
"icons/Icon-512.png": "96e752610906ba2a93c65f8abe1645f1",
"icons/Icon-maskable-512.png": "301a7604d45b3e739efc881eb04896ea",
"icons/Icon-192.png": "ac9a721a12bbc803b44f645561ecb1e1"};
// The application shell files that are downloaded before a service worker can
// start.
const CORE = ["main.dart.js",
"index.html",
"assets/AssetManifest.json",
"assets/FontManifest.json"];

// During install, the TEMP cache is populated with the application shell files.
self.addEventListener("install", (event) => {
  self.skipWaiting();
  return event.waitUntil(
    caches.open(TEMP).then((cache) => {
      return cache.addAll(
        CORE.map((value) => new Request(value, {'cache': 'reload'})));
    })
  );
});
// During activate, the cache is populated with the temp files downloaded in
// install. If this service worker is upgrading from one with a saved
// MANIFEST, then use this to retain unchanged resource files.
self.addEventListener("activate", function(event) {
  return event.waitUntil(async function() {
    try {
      var contentCache = await caches.open(CACHE_NAME);
      var tempCache = await caches.open(TEMP);
      var manifestCache = await caches.open(MANIFEST);
      var manifest = await manifestCache.match('manifest');
      // When there is no prior manifest, clear the entire cache.
      if (!manifest) {
        await caches.delete(CACHE_NAME);
        contentCache = await caches.open(CACHE_NAME);
        for (var request of await tempCache.keys()) {
          var response = await tempCache.match(request);
          await contentCache.put(request, response);
        }
        await caches.delete(TEMP);
        // Save the manifest to make future upgrades efficient.
        await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
        // Claim client to enable caching on first launch
        self.clients.claim();
        return;
      }
      var oldManifest = await manifest.json();
      var origin = self.location.origin;
      for (var request of await contentCache.keys()) {
        var key = request.url.substring(origin.length + 1);
        if (key == "") {
          key = "/";
        }
        // If a resource from the old manifest is not in the new cache, or if
        // the MD5 sum has changed, delete it. Otherwise the resource is left
        // in the cache and can be reused by the new service worker.
        if (!RESOURCES[key] || RESOURCES[key] != oldManifest[key]) {
          await contentCache.delete(request);
        }
      }
      // Populate the cache with the app shell TEMP files, potentially overwriting
      // cache files preserved above.
      for (var request of await tempCache.keys()) {
        var response = await tempCache.match(request);
        await contentCache.put(request, response);
      }
      await caches.delete(TEMP);
      // Save the manifest to make future upgrades efficient.
      await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
      // Claim client to enable caching on first launch
      self.clients.claim();
      return;
    } catch (err) {
      // On an unhandled exception the state of the cache cannot be guaranteed.
      console.error('Failed to upgrade service worker: ' + err);
      await caches.delete(CACHE_NAME);
      await caches.delete(TEMP);
      await caches.delete(MANIFEST);
    }
  }());
});
// The fetch handler redirects requests for RESOURCE files to the service
// worker cache.
self.addEventListener("fetch", (event) => {
  if (event.request.method !== 'GET') {
    return;
  }
  var origin = self.location.origin;
  var key = event.request.url.substring(origin.length + 1);
  // Redirect URLs to the index.html
  if (key.indexOf('?v=') != -1) {
    key = key.split('?v=')[0];
  }
  if (event.request.url == origin || event.request.url.startsWith(origin + '/#') || key == '') {
    key = '/';
  }
  // If the URL is not the RESOURCE list then return to signal that the
  // browser should take over.
  if (!RESOURCES[key]) {
    return;
  }
  // If the URL is the index.html, perform an online-first request.
  if (key == '/') {
    return onlineFirst(event);
  }
  event.respondWith(caches.open(CACHE_NAME)
    .then((cache) =>  {
      return cache.match(event.request).then((response) => {
        // Either respond with the cached resource, or perform a fetch and
        // lazily populate the cache only if the resource was successfully fetched.
        return response || fetch(event.request).then((response) => {
          if (response && Boolean(response.ok)) {
            cache.put(event.request, response.clone());
          }
          return response;
        });
      })
    })
  );
});
self.addEventListener('message', (event) => {
  // SkipWaiting can be used to immediately activate a waiting service worker.
  // This will also require a page refresh triggered by the main worker.
  if (event.data === 'skipWaiting') {
    self.skipWaiting();
    return;
  }
  if (event.data === 'downloadOffline') {
    downloadOffline();
    return;
  }
});
// Download offline will check the RESOURCES for all files not in the cache
// and populate them.
async function downloadOffline() {
  var resources = [];
  var contentCache = await caches.open(CACHE_NAME);
  var currentContent = {};
  for (var request of await contentCache.keys()) {
    var key = request.url.substring(origin.length + 1);
    if (key == "") {
      key = "/";
    }
    currentContent[key] = true;
  }
  for (var resourceKey of Object.keys(RESOURCES)) {
    if (!currentContent[resourceKey]) {
      resources.push(resourceKey);
    }
  }
  return contentCache.addAll(resources);
}
// Attempt to download the resource online before falling back to
// the offline cache.
function onlineFirst(event) {
  return event.respondWith(
    fetch(event.request).then((response) => {
      return caches.open(CACHE_NAME).then((cache) => {
        cache.put(event.request, response.clone());
        return response;
      });
    }).catch((error) => {
      return caches.open(CACHE_NAME).then((cache) => {
        return cache.match(event.request).then((response) => {
          if (response != null) {
            return response;
          }
          throw error;
        });
      });
    })
  );
}
